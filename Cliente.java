public class Cliente{

      private String nome;
      private String numeroDaConta;
      private String cpf;
      private double saldo;
 
      public void setNome (String umNome){
	  nome = umNome;
      }
      
      public String getNome(){
	  return nome;
      }

      public void setNumeroDaConta (String umNumeroDaConta){
	  numeroDaConta = umNumeroDaConta;
      }

      public String getNumeroDaConta(){
	  return numeroDaConta;
      }

      
      public void setCpf (String umCpf){
	  cpf = umCpf;
      }
      
      public String getCpf(){
	  return cpf;
      }
      
      public void setSaldo (double umSaldo){
	  saldo = umSaldo;
      }

       public double getSaldo(){
	  return saldo;
      }
      
}
